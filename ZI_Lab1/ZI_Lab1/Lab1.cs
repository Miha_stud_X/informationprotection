﻿using System;
using System.Windows.Forms;

namespace ZI_Labs
{
    public class Lab1
    {
        //собираем строки в массив всех символов
        public char[] leters;

        public Lab1(string alfabit, string specLet)
        {
            this.leters = (alfabit + alfabit.ToUpper() + specLet + "\n").ToCharArray();
        }

        //получение массива int соответсующего символам ключевого слова
        private int[] getKeyInt(char[] key)
        {
            int[] keyInt = new int[key.Length];
            for (int i = 0; i < key.Length; i++)
            {
                keyInt[i] = Array.IndexOf(leters, key[i]);
            }
            return keyInt;
        }

        //проверка поля ключа на пустоту
        public string keyCheck(string text)
        {
            if (text.Length < 1)
                return "Не введён ключ";
            else
                return null;
        }

        //шифровка
        public string shifr(string text_in, char[] key)
        {
            int[] keyInt = getKeyInt(key);
            int delta = 0;
            char[] text = text_in.ToCharArray();
            for (int i = 0, j = 0; i < text.Length; i++, j++)
            {
                if (j == keyInt.Length) j = 0;
                delta = Array.IndexOf(leters, text[i]) + 1 + (keyInt[j] + 1);
                delta %= leters.Length;
                text[i] = leters[delta - 1];
            }
            return new string(text);
        }

        //расшифровка
        public string deShifr(string text_in, char[] key)
        {
            int[] keyInt = getKeyInt(key);
            int delta = 0;
            char[] text = text_in.ToCharArray();
            for (int i = 0, j = 0; i < text.Length; i++, j++)
            {
                if (j == keyInt.Length) j = 0;
                delta = Array.IndexOf(leters, text[i]) + 1 - (keyInt[j] + 1);
                if (delta < 0) delta += leters.Length;
                text[i] = leters[delta - 1];
            }
            return new string(text);
        }

        //отображение промежуточных этапов в таблице
        public void print_etaps(string text_in, char[] key, bool shifr, DataGridView dataGridView1)
        {
            dataGridView1.Visible = true;
            dataGridView1.Rows.Clear();
            dataGridView1.Columns.Clear();

            int[] keyInt = getKeyInt(key);
            int delta = 0;
            char[] text = text_in.ToCharArray();

            for (int i = 0; i < text.Length; i++)
            {
                dataGridView1.Columns.Add(i.ToString(), text[i].ToString());
            }

            dataGridView1.Rows.Add();
            if (shifr)
                dataGridView1.Rows[0].HeaderCell.Value = "Код исходный";
            else
                dataGridView1.Rows[0].HeaderCell.Value = "Код шифра";
            for (int i = 0, j = 0; i < text.Length; i++, j++)
            {
                if (j == keyInt.Length) j = 0;
                delta = keyInt[j];
                dataGridView1[i, 0].Value = Array.IndexOf(leters, text[i]) + 1;
            }

            dataGridView1.Rows.Add();
            dataGridView1.Rows[1].HeaderCell.Value = "Сдвиг по ключу";
            for (int i = 0, j = 0; i < text.Length; i++, j++)
            {
                if (j == keyInt.Length) j = 0;
                delta = keyInt[j];
                dataGridView1[i, 1].Value = keyInt[j] + 1;
            }

            dataGridView1.Rows.Add();
            if (shifr)
                dataGridView1.Rows[2].HeaderCell.Value = "Код шифра";
            else
                dataGridView1.Rows[2].HeaderCell.Value = "Код исходный";
            for (int i = 0, j = 0; i < text.Length; i++, j++)
            {
                if (j == keyInt.Length) j = 0;
                delta = keyInt[j];
                if (shifr)
                    dataGridView1[i, 2].Value = Array.IndexOf(leters, text[i]) + 1 + keyInt[j] + 1;
                else
                    dataGridView1[i, 2].Value = (Array.IndexOf(leters, text[i]) + 1) - (keyInt[j] + 1);
            }
        }
    }
}

﻿namespace ZI_Labs
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.labelTabel = new System.Windows.Forms.Label();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.errorLabel = new System.Windows.Forms.Label();
            this.richTextBox2 = new System.Windows.Forms.RichTextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.keyTextBox = new System.Windows.Forms.TextBox();
            this.richTextBox1 = new System.Windows.Forms.RichTextBox();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.файлToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.openFileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.inLWOpenFile = new System.Windows.Forms.ToolStripMenuItem();
            this.inRWOpenFile = new System.Windows.Forms.ToolStripMenuItem();
            this.saveFileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.LSafeBtn = new System.Windows.Forms.ToolStripMenuItem();
            this.RSafeBtn = new System.Windows.Forms.ToolStripMenuItem();
            this.saveFileAsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.LasSaveBTN = new System.Windows.Forms.ToolStripMenuItem();
            this.RasSaveBTN = new System.Windows.Forms.ToolStripMenuItem();
            this.MainShifrToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.gammItem = new System.Windows.Forms.ToolStripMenuItem();
            this.shifr1TSMItem = new System.Windows.Forms.ToolStripMenuItem();
            this.deShifr1TSMItem = new System.Windows.Forms.ToolStripMenuItem();
            this.changeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.shifr2TSMItem = new System.Windows.Forms.ToolStripMenuItem();
            this.deShifr2TSMItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.saveFileDialog1 = new System.Windows.Forms.SaveFileDialog();
            this.erorRichTextBox = new System.Windows.Forms.RichTextBox();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Font = new System.Drawing.Font("Microsoft JhengHei", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tabControl1.Location = new System.Drawing.Point(12, 12);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(626, 591);
            this.tabControl1.TabIndex = 0;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.erorRichTextBox);
            this.tabPage1.Controls.Add(this.labelTabel);
            this.tabPage1.Controls.Add(this.dataGridView1);
            this.tabPage1.Controls.Add(this.label3);
            this.tabPage1.Controls.Add(this.label2);
            this.tabPage1.Controls.Add(this.errorLabel);
            this.tabPage1.Controls.Add(this.richTextBox2);
            this.tabPage1.Controls.Add(this.label1);
            this.tabPage1.Controls.Add(this.keyTextBox);
            this.tabPage1.Controls.Add(this.richTextBox1);
            this.tabPage1.Controls.Add(this.menuStrip1);
            this.tabPage1.Location = new System.Drawing.Point(4, 29);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(618, 558);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Лабораторная №1-2";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // labelTabel
            // 
            this.labelTabel.AutoSize = true;
            this.labelTabel.Font = new System.Drawing.Font("Microsoft JhengHei", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelTabel.Location = new System.Drawing.Point(7, 412);
            this.labelTabel.Name = "labelTabel";
            this.labelTabel.Size = new System.Drawing.Size(212, 17);
            this.labelTabel.TabIndex = 9;
            this.labelTabel.Text = "Поэтапное описание процесса";
            // 
            // dataGridView1
            // 
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(10, 432);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(602, 120);
            this.dataGridView1.TabIndex = 8;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft JhengHei", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(309, 333);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(46, 17);
            this.label3.TabIndex = 7;
            this.label3.Text = "Шифр";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft JhengHei", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(6, 333);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(111, 17);
            this.label2.TabIndex = 6;
            this.label2.Text = "Исходный текст";
            // 
            // errorLabel
            // 
            this.errorLabel.AutoSize = true;
            this.errorLabel.Location = new System.Drawing.Point(323, 371);
            this.errorLabel.Name = "errorLabel";
            this.errorLabel.Size = new System.Drawing.Size(0, 20);
            this.errorLabel.TabIndex = 5;
            // 
            // richTextBox2
            // 
            this.richTextBox2.Font = new System.Drawing.Font("Microsoft JhengHei", 14F);
            this.richTextBox2.Location = new System.Drawing.Point(312, 34);
            this.richTextBox2.Name = "richTextBox2";
            this.richTextBox2.Size = new System.Drawing.Size(300, 296);
            this.richTextBox2.TabIndex = 4;
            this.richTextBox2.Text = "";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 371);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(61, 20);
            this.label1.TabIndex = 3;
            this.label1.Text = "КЛЮЧ:";
            // 
            // keyTextBox
            // 
            this.keyTextBox.Location = new System.Drawing.Point(73, 368);
            this.keyTextBox.Name = "keyTextBox";
            this.keyTextBox.Size = new System.Drawing.Size(216, 29);
            this.keyTextBox.TabIndex = 2;
            // 
            // richTextBox1
            // 
            this.richTextBox1.Font = new System.Drawing.Font("Microsoft JhengHei", 14F);
            this.richTextBox1.Location = new System.Drawing.Point(6, 34);
            this.richTextBox1.Name = "richTextBox1";
            this.richTextBox1.Size = new System.Drawing.Size(300, 296);
            this.richTextBox1.TabIndex = 1;
            this.richTextBox1.Text = "";
            // 
            // menuStrip1
            // 
            this.menuStrip1.BackColor = System.Drawing.Color.Gainsboro;
            this.menuStrip1.Font = new System.Drawing.Font("Segoe UI", 11F);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.файлToolStripMenuItem,
            this.MainShifrToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(3, 3);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(612, 28);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // файлToolStripMenuItem
            // 
            this.файлToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.openFileToolStripMenuItem,
            this.saveFileToolStripMenuItem,
            this.saveFileAsToolStripMenuItem});
            this.файлToolStripMenuItem.Name = "файлToolStripMenuItem";
            this.файлToolStripMenuItem.Size = new System.Drawing.Size(57, 24);
            this.файлToolStripMenuItem.Text = "Файл";
            // 
            // openFileToolStripMenuItem
            // 
            this.openFileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.inLWOpenFile,
            this.inRWOpenFile});
            this.openFileToolStripMenuItem.Name = "openFileToolStripMenuItem";
            this.openFileToolStripMenuItem.Size = new System.Drawing.Size(178, 24);
            this.openFileToolStripMenuItem.Text = "Открыть";
            // 
            // inLWOpenFile
            // 
            this.inLWOpenFile.Name = "inLWOpenFile";
            this.inLWOpenFile.Size = new System.Drawing.Size(180, 24);
            this.inLWOpenFile.Text = "В левое окно";
            this.inLWOpenFile.Click += new System.EventHandler(this.inLWOpenFile_Click);
            // 
            // inRWOpenFile
            // 
            this.inRWOpenFile.Name = "inRWOpenFile";
            this.inRWOpenFile.Size = new System.Drawing.Size(180, 24);
            this.inRWOpenFile.Text = "В правое окно";
            this.inRWOpenFile.Click += new System.EventHandler(this.inRWOpenFile_Click);
            // 
            // saveFileToolStripMenuItem
            // 
            this.saveFileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.LSafeBtn,
            this.RSafeBtn});
            this.saveFileToolStripMenuItem.Name = "saveFileToolStripMenuItem";
            this.saveFileToolStripMenuItem.Size = new System.Drawing.Size(178, 24);
            this.saveFileToolStripMenuItem.Text = "Сохранить";
            // 
            // LSafeBtn
            // 
            this.LSafeBtn.Name = "LSafeBtn";
            this.LSafeBtn.Size = new System.Drawing.Size(169, 24);
            this.LSafeBtn.Text = "Левое окно";
            this.LSafeBtn.Click += new System.EventHandler(this.LSafeBtn_Click);
            // 
            // RSafeBtn
            // 
            this.RSafeBtn.Name = "RSafeBtn";
            this.RSafeBtn.Size = new System.Drawing.Size(169, 24);
            this.RSafeBtn.Text = "Правое окно";
            this.RSafeBtn.Click += new System.EventHandler(this.RSafeBtn_Click);
            // 
            // saveFileAsToolStripMenuItem
            // 
            this.saveFileAsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.LasSaveBTN,
            this.RasSaveBTN});
            this.saveFileAsToolStripMenuItem.Name = "saveFileAsToolStripMenuItem";
            this.saveFileAsToolStripMenuItem.Size = new System.Drawing.Size(178, 24);
            this.saveFileAsToolStripMenuItem.Text = "Сохранить как";
            // 
            // LasSaveBTN
            // 
            this.LasSaveBTN.Name = "LasSaveBTN";
            this.LasSaveBTN.Size = new System.Drawing.Size(169, 24);
            this.LasSaveBTN.Text = "Левое окно";
            this.LasSaveBTN.Click += new System.EventHandler(this.LasSaveBTN_Click);
            // 
            // RasSaveBTN
            // 
            this.RasSaveBTN.Name = "RasSaveBTN";
            this.RasSaveBTN.Size = new System.Drawing.Size(169, 24);
            this.RasSaveBTN.Text = "Правое окно";
            this.RasSaveBTN.Click += new System.EventHandler(this.RasSaveBTN_Click);
            // 
            // MainShifrToolStripMenuItem
            // 
            this.MainShifrToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.gammItem,
            this.changeToolStripMenuItem});
            this.MainShifrToolStripMenuItem.Name = "MainShifrToolStripMenuItem";
            this.MainShifrToolStripMenuItem.Size = new System.Drawing.Size(114, 24);
            this.MainShifrToolStripMenuItem.Text = "Шифрование";
            // 
            // gammItem
            // 
            this.gammItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.shifr1TSMItem,
            this.deShifr1TSMItem});
            this.gammItem.Name = "gammItem";
            this.gammItem.Size = new System.Drawing.Size(229, 24);
            this.gammItem.Text = "метод гаммирования";
            // 
            // shifr1TSMItem
            // 
            this.shifr1TSMItem.Name = "shifr1TSMItem";
            this.shifr1TSMItem.Size = new System.Drawing.Size(175, 24);
            this.shifr1TSMItem.Text = "Шифровать";
            this.shifr1TSMItem.Click += new System.EventHandler(this.shifr1TSMItem_Click);
            // 
            // deShifr1TSMItem
            // 
            this.deShifr1TSMItem.Name = "deShifr1TSMItem";
            this.deShifr1TSMItem.Size = new System.Drawing.Size(175, 24);
            this.deShifr1TSMItem.Text = "Дешифровать";
            this.deShifr1TSMItem.Click += new System.EventHandler(this.deShifr1TSMItem_Click);
            // 
            // changeToolStripMenuItem
            // 
            this.changeToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.shifr2TSMItem,
            this.deShifr2TSMItem});
            this.changeToolStripMenuItem.Name = "changeToolStripMenuItem";
            this.changeToolStripMenuItem.Size = new System.Drawing.Size(229, 24);
            this.changeToolStripMenuItem.Text = "метод перестановок";
            // 
            // shifr2TSMItem
            // 
            this.shifr2TSMItem.Name = "shifr2TSMItem";
            this.shifr2TSMItem.Size = new System.Drawing.Size(175, 24);
            this.shifr2TSMItem.Text = "Шифровать";
            this.shifr2TSMItem.Click += new System.EventHandler(this.shifr2TSMItem_Click);
            // 
            // deShifr2TSMItem
            // 
            this.deShifr2TSMItem.Name = "deShifr2TSMItem";
            this.deShifr2TSMItem.Size = new System.Drawing.Size(175, 24);
            this.deShifr2TSMItem.Text = "Дешифровать";
            this.deShifr2TSMItem.Click += new System.EventHandler(this.deShifr2TSMItem_Click);
            // 
            // tabPage2
            // 
            this.tabPage2.Location = new System.Drawing.Point(4, 29);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(618, 558);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Лабораторная №3";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // erorRichTextBox
            // 
            this.erorRichTextBox.Font = new System.Drawing.Font("Microsoft JhengHei", 14F);
            this.erorRichTextBox.Location = new System.Drawing.Point(10, 432);
            this.erorRichTextBox.Name = "erorRichTextBox";
            this.erorRichTextBox.Size = new System.Drawing.Size(602, 120);
            this.erorRichTextBox.TabIndex = 10;
            this.erorRichTextBox.Text = "";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.ClientSize = new System.Drawing.Size(650, 615);
            this.Controls.Add(this.tabControl1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Form1";
            this.Text = "Защита информации. АВТ-613";
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem файлToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem openFileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem saveFileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem saveFileAsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem MainShifrToolStripMenuItem;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.RichTextBox richTextBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox keyTextBox;
        private System.Windows.Forms.RichTextBox richTextBox2;
        private System.Windows.Forms.Label errorLabel;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.SaveFileDialog saveFileDialog1;
        private System.Windows.Forms.ToolStripMenuItem LSafeBtn;
        private System.Windows.Forms.ToolStripMenuItem RSafeBtn;
        private System.Windows.Forms.ToolStripMenuItem LasSaveBTN;
        private System.Windows.Forms.ToolStripMenuItem RasSaveBTN;
        private System.Windows.Forms.ToolStripMenuItem inLWOpenFile;
        private System.Windows.Forms.ToolStripMenuItem inRWOpenFile;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.Label labelTabel;
        private System.Windows.Forms.ToolStripMenuItem gammItem;
        private System.Windows.Forms.ToolStripMenuItem shifr1TSMItem;
        private System.Windows.Forms.ToolStripMenuItem deShifr1TSMItem;
        private System.Windows.Forms.ToolStripMenuItem changeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem shifr2TSMItem;
        private System.Windows.Forms.ToolStripMenuItem deShifr2TSMItem;
        private System.Windows.Forms.RichTextBox erorRichTextBox;
    }
}


﻿using System;
using System.Windows.Forms;

namespace ZI_Labs
{
    public partial class Form1 : Form
    {
        string filename = null;
        private static string alfabit = "абвгдеёжзийклмнопрстуфхцчшщьыъэюя";
        private static string specLet = " ,./%@$&#1234567890";
        Lab1 lab1 = new Lab1(alfabit, specLet);

        public Form1()
        {
            InitializeComponent();
            saveFileToolStripMenuItem.Enabled = false;
            dataGridView1.Visible = false;
            labelTabel.Visible = false;
            erorRichTextBox.Visible = false;
            openFileDialog1.Filter = "Text files(*.txt)|*.txt|All files(*.*)|*.*";
            saveFileDialog1.Filter = "Text files(*.txt)|*.txt|All files(*.*)|*.*";
        }

        //--------------------Click listeners---------------------------------

        //Открытие файла в левое окно
        private void inLWOpenFile_Click(object sender, EventArgs e)
        {
            if (openFileDialog1.ShowDialog() == DialogResult.Cancel)
                return;
            filename = openFileDialog1.FileName;
            string fileText = System.IO.File.ReadAllText(filename);
            richTextBox1.Text = fileText;
            saveFileToolStripMenuItem.Enabled = true;
            dataGridView1.Visible = false;
        }

        //Открытие файла в правое окно
        private void inRWOpenFile_Click(object sender, EventArgs e)
        {
            if (openFileDialog1.ShowDialog() == DialogResult.Cancel)
                return;
            filename = openFileDialog1.FileName;
            string fileText = System.IO.File.ReadAllText(filename);
            richTextBox2.Text = fileText;
            saveFileToolStripMenuItem.Enabled = true;
            dataGridView1.Visible = false;
        }

        //Сохранение в файл из левого окна
        private void LSafeBtn_Click(object sender, EventArgs e)
        {
            if (filename != null)
            {
                System.IO.File.WriteAllText(filename, richTextBox1.Text);
            }
        }

        //Сохранение в файл из правого окна
        private void RSafeBtn_Click(object sender, EventArgs e)
        {
            if (filename != null)
            {
                System.IO.File.WriteAllText(filename, richTextBox2.Text);
            }
        }

        //Сохранение в новый файл из левого окна
        private void LasSaveBTN_Click(object sender, EventArgs e)
        {
            if (saveFileDialog1.ShowDialog() == DialogResult.Cancel)
                return;
            string filename2 = saveFileDialog1.FileName;
            System.IO.File.WriteAllText(filename2, richTextBox1.Text);
        }

        //Сохранение в новый файл из правого окна
        private void RasSaveBTN_Click(object sender, EventArgs e)
        {
            if (saveFileDialog1.ShowDialog() == DialogResult.Cancel)
                return;
            string filename2 = saveFileDialog1.FileName;
            System.IO.File.WriteAllText(filename2, richTextBox2.Text);
        }
        
        //-------------------ЛАБА 1-------------------------------------------
        //шифровка
        private void shifr1TSMItem_Click(object sender, EventArgs e)
        {
            labelTabel.Visible = false;
            dataGridView1.Visible = false;
            errorLabel.Text = string.Empty;
            erorRichTextBox.Visible = false;
            string err = lab1.keyCheck(keyTextBox.Text);
            if (err != null)
            {
                errorLabel.Text = err;
                return;
            }
            labelTabel.Visible = true;
            lab1.print_etaps(richTextBox1.Text, keyTextBox.Text.ToCharArray(), true, dataGridView1);
            labelTabel.Text = "Поэтапное описание процесса";
            richTextBox2.Text = lab1.shifr(richTextBox1.Text, keyTextBox.Text.ToCharArray());
        }

        //дешифровка
        private void deShifr1TSMItem_Click(object sender, EventArgs e)
        {
            labelTabel.Visible = false;
            dataGridView1.Visible = false;
            errorLabel.Text = string.Empty;
            erorRichTextBox.Visible = false;
            string err = lab1.keyCheck(keyTextBox.Text);
            if (err != null)
            {
                errorLabel.Text = err;
                return;
            }
            labelTabel.Visible = true;
            lab1.print_etaps(richTextBox2.Text, keyTextBox.Text.ToCharArray(), false, dataGridView1);
            labelTabel.Text = "Поэтапное описание процесса";
            richTextBox1.Text = lab1.deShifr(richTextBox2.Text, keyTextBox.Text.ToCharArray());
        }

        //-----------------------------------------------------------------

        //-------------------ЛАБА 2-------------------------------------------
        //шифровка
        private void shifr2TSMItem_Click(object sender, EventArgs e)
        {
            labelTabel.Visible = false;
            dataGridView1.Visible = false;
            errorLabel.Text = string.Empty;
            erorRichTextBox.Visible = false;
            string err = lab1.keyCheck(keyTextBox.Text);
            if (err != null)
            {
                errorLabel.Text = err;
                return;
            }
            Lab2 lab2 = new Lab2(keyTextBox.Text, alfabit+specLet);
            richTextBox2.Text = lab2.Encrypt(richTextBox1.Text);
            if (lab2.Error.Count > 0)
            {
                erorRichTextBox.Visible = true;
                labelTabel.Visible = true;
                labelTabel.Text = "Ошибки:";
            }
            while (lab2.Error.Count > 0)
            {
                erorRichTextBox.Text += lab2.Error.Dequeue() + Environment.NewLine;
            }
        }
        //дешифровка
        private void deShifr2TSMItem_Click(object sender, EventArgs e)
        {
            labelTabel.Visible = false;
            dataGridView1.Visible = false;
            errorLabel.Text = string.Empty;
            erorRichTextBox.Visible = false;
            string err = lab1.keyCheck(keyTextBox.Text);
            if (err != null)
            {
                errorLabel.Text = err;
                return;
            }
            Lab2 lab2 = new Lab2(keyTextBox.Text, alfabit+specLet);
            richTextBox1.Text = lab2.Decrypt(richTextBox2.Text);
            if (lab2.Error.Count > 0)
            {
                erorRichTextBox.Visible = true;
                labelTabel.Visible = true;
                labelTabel.Text = "Ошибки:";
            }
            while (lab2.Error.Count > 0)
            {
                erorRichTextBox.Text += lab2.Error.Dequeue() + Environment.NewLine;
            }
        }

        //-----------------------------------------------------------------
    }

}

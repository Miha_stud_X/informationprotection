﻿using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace ZI_Labs
{
    class Lab2
    {
        private string keyString;
        private string alphabet;
        private Queue<string> error;
        private int n;

        public Lab2(string keyString, string alphabet)
        {
            string alphabetBuf = string.Empty;

            Regex regex = new Regex(@"(\w*)");
            MatchCollection matches = regex.Matches(alphabet);
            if (matches.Count > 0)
            {
                foreach (Match match in matches)
                    alphabetBuf += match.Value;
            }

            Alphabet = alphabetBuf.ToUpper() + alphabet;
            N = Alphabet.Length;
            KeyString = keyString.ToUpper();
            error = new Queue<string>();
        }

        public string Encrypt(string source)
        {
            if (source == null)
            {
                error.Enqueue("Отсутствует исходная строка!");
                return null;
            }

            string result = string.Empty;
            int j, l, k, i;

            j = 0;
            while(j < source.Length)       //МЕТОД ПЕРЕСТАНОВКИ      ВЕСНА    
            {                               //абвгдежзиклмнопрстуфхцчшщъыьэюя
                l = 0;
                while (l < keyString.Length && j < source.Length)
                {
                    i = Alphabet.IndexOf(source[j]);
                    k = Alphabet.IndexOf(keyString[l]);
                    if (i < 0 || k < 0)
                    {
                        error.Enqueue("Символы отсутствуют в алфавите!");
                        return null;
                    }
                    if (i < N - k)
                    {
                        result += Alphabet[i + k];
                    }
                    else
                    {
                        result += Alphabet[i - N + k];
                    }
                    j++;
                    l++;
                }
            }
            return result;
        }

        public string Decrypt(string source)
        {
            if(source == null)
            {
                error.Enqueue("Отсутствует исходная строка!");
                return null;
            }

            string result = string.Empty;
            int j, l, k, i;

            j = 0;
            while (j < source.Length)       //МЕТОД ПЕРЕСТАНОВКИ      ВЕСНА    
            {                               //абвгдежзиклмнопрстуфхцчшщъыьэюя
                l = 0;
                while (l < keyString.Length && j < source.Length)//m-12
                {
                    i = Alphabet.IndexOf(source[j]);//14
                    k = Alphabet.IndexOf(keyString[l]);//3
                    if (i < 0 || k < 0)
                    {
                        error.Enqueue("Символы отсутствуют в алфавите!");
                        return null;
                    }
                    if (i >= k)
                    {
                        result += Alphabet[i - k];
                    }
                    else
                    {
                        result += Alphabet[N - k + i];
                    }
                    j++;
                    l++;
                }
            }
            return result;
        }

        public string KeyString { get => keyString; set => keyString = value; }
        public string Alphabet { get => alphabet; set => alphabet = value; }
        private int N { get => n; set => n = value; }
        public Queue<string> Error { get => error; }
    }
}
